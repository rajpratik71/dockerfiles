# xc16 Docker Image

[![website badge](https://img.shields.io/badge/website-fstlisboa.com-blue?style=flat-square)](https://fstlisboa.com/) [![gitlab](https://img.shields.io/badge/GitLab-orange?style=flat-square&logo=gitlab)](https://gitlab.com/projectofst/dockerfiles)

Based on Ubuntu 20.04. There is also an arch version.



## How to use

To pull the image:

```
docker pull fstlx/xc16:latest
```

or if you want to run it attached:

```
docker run -it fstlx/xc16:latest
```
